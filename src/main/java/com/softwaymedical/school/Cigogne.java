package com.softwaymedical.school;

public class Cigogne extends Animal {
	// mettre tous les trucs static en 1er pour swm

	public Cigogne(String nom) {
		super(nom);
		this.setCouleur(Couleur.BLANCHE);

	}

	// @Override
	// public Couleur getCouleur() {
	// return couleur;
	// }

	@Override
	public String presenteToi() {

		return "Je suis une " + this.toString().toLowerCase() + " " + this.getCouleur() + " et je m'appelle " + this.getNom() + ".";
	}

	@Override
	public String toString() {
		return "Cigogne";
	}

}
