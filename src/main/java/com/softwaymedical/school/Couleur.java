package com.softwaymedical.school;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public enum Couleur {

	VERT, JAUNE, ROUGE, BLEU, ORANGE, ROSE, MARON_SALE, BLANCHE, GRIS;

	private static final Random PRNG = new Random();

	public Couleur randomCouleur() {
		List<Couleur> couleurs = new ArrayList<Couleur>();
		couleurs.addAll(Arrays.asList(values()));
		couleurs.remove(MARON_SALE);

		return couleurs.get(PRNG.nextInt(couleurs.size()));
	}
}
