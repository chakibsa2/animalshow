package com.softwaymedical.school;

public class Chat extends Animal implements Jouable {
	private boolean content = false;

	public Chat(String nom, Couleur couleur) {
		super(nom, couleur);

	}

	@Override
	public boolean jouer() {
		// void

		return content = true;

	}

	@Override
	public String presenteToi() {
		if (content) {
			this.content = false;

			return "Je suis un " + this.toString().toLowerCase() + " " + this.getCouleur() + " et je m'appelle " + this.getNom() + ".";

		} else
			return "...";

	}

	@Override
	public String toString() {
		return "Chat";
	}

}
