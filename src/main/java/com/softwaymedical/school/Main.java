package com.softwaymedical.school;

public class Main {

	public static void main(String[] args) {
		Cigogne ci = new Cigogne("toto");
		System.out.println(ci.presenteToi());
		System.out.println(".......................................");
		Grenouille gi = new Grenouille("moomo");
		System.out.println(gi.presenteToi());
		System.out.println(".......................................");

		Chat ch = new Chat("lolol", Couleur.BLEU);
		System.out.println(ch.presenteToi());
		ch.jouer();
		System.out.println(ch.presenteToi());
		System.out.println(ch.presenteToi());
		System.out.println(".......................................");
		Camelleon ca = new Camelleon("vovo", Couleur.ROUGE);
		System.out.println(ca.presenteToi());

		ca.jouer();
		System.out.println(ca.presenteToi());
		System.out.println(".......................................");
		Elephant el = new Elephant("babar");
		System.out.println(el.presenteToi());
		try {
			el.jouer();
		} catch (Exception e) {

			System.err.println(e.getMessage());
		}
		System.out.println(".......................................");
		el.lancerBoue(ci);
		System.out.println(ci.presenteToi());
		System.out.println(".......................................");
		el.lancerBoue(gi);
		System.out.println(gi.presenteToi());
		System.out.println(".......................................");
		el.lancerBoue(ch);
		System.out.println(ch.presenteToi());
		ch.jouer();
		System.out.println(ch.presenteToi());
		System.out.println(".......................................");
		Elephant ell = new Elephant("babarrrrr");
		System.out.println(ell.presenteToi());
		el.lancerBoue(ell);
		System.out.println(ell.presenteToi());
		System.out.println(".......................................");
		System.out.println(ca.presenteToi());

		ca.jouer();
		System.out.println(ca.presenteToi());
		el.lancerBoue(ca);
		System.out.println(ca.presenteToi());
		ca.jouer();
		System.out.println(ca.presenteToi());

	}
}
