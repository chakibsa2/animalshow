package com.softwaymedical.school;

public class Camelleon extends Animal implements Jouable {

	@SuppressWarnings("unused")
	private boolean content = false;

	public Camelleon(String nom, Couleur couleur) {
		super(nom, couleur);

	}

	@Override
	public boolean jouer() {
		if (this.getCouleur() != Couleur.MARON_SALE) {

			this.setCouleur(couleur.randomCouleur());
		}
		return content = true;

	}

	@Override
	public String presenteToi() {
		content = false;

		return "Je suis un " + this.toString().toLowerCase() + " " + this.getCouleur() + " et je m'appelle " + this.getNom() + ".";

	}

	@Override
	public String toString() {
		return "Camelleon";
	}

}
