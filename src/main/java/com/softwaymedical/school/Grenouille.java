package com.softwaymedical.school;

public class Grenouille extends Animal {

	// private final String couleur = "verte";

	public Grenouille(String nom) {
		super(nom);

	}

	@Override
	public String presenteToi() {

		return "Je suis une " + this.toString().toLowerCase() + " " + "verte" + " et je m'appelle " + this.getNom() + ".";
	}

	@Override
	public String toString() {
		return "Grenouille";
	}

}
