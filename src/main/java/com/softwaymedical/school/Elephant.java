package com.softwaymedical.school;

public class Elephant extends Animal implements Jouable {

	public Elephant(String nom) {
		super(nom);
		this.setCouleur(Couleur.GRIS);

	}

	@Override
	public Couleur getCouleur() {
		return couleur;
	}

	@Override
	public boolean jouer() throws Exception {

		throw new Exception("Vous venez de vous faire écraser par un éléphant.");

	}

	@Override
	public String presenteToi() {

		return "JE SUIS UN  " + this.toString() + " " + this.getCouleur() + " ET JE M'APPELLE " + this.getNom().toUpperCase() + ".";
	}

	@Override
	public String toString() {
		return "ELEPHANT";
	}

	public void lancerBoue(Animal animal) {
		animal.setCouleur(Couleur.MARON_SALE);

	}

}
